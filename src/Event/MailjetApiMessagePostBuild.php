<?php

namespace Drupal\mailjet_api\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Allow developers to alter the mailjet message array after build.
 */
class MailjetApiMessagePostBuild extends Event {

  /**
   * The Mailjet message just before it is added to the body of the API call.
   *
   * @var array
   */
  protected $message;

  /**
   * MailjetApiMessagePostBuild constructor.
   *
   * @param array $message
   */
  public function __construct(array $message) {
    $this->message = $message;
  }

  /**
   * Get the finished mailjet message array.
   *
   * @return array
   */
  public function getMessage(): array {
    return $this->message;
  }

  /**
   * Set the final mailjet message array.
   *
   * @param array
   */
  public function setMessage(string $message): void {
    $this->message = $message;
  }

}
