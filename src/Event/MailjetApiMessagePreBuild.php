<?php

namespace Drupal\mailjet_api\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Allow developers to alter the core message array before build.
 *
 * May be used instead of hook_mail_alter to do such things as add a TemplateId.
 */
class MailjetApiMessagePreBuild extends Event {

  /**
   * The core message array just before it is used to create a mailjet message.
   *
   * @var array
   */
  protected $message;

  /**
   * MailjetApiMessagePreBuild constructor.
   *
   * @param array $message
   */
  public function __construct(array $message) {
    $this->message = $message;
  }

  /**
   * Get the core message array.
   *
   * @return array
   */
  public function getMessage(): array {
    return $this->message;
  }

  /**
   * Set the core message array.
   *
   * @param array
   */
  public function setMessage(string $message): void {
    $this->message = $message;
  }

}
