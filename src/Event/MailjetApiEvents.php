<?php

namespace Drupal\mailjet_api\Event;

final class MailjetApiEvents {

  const MAILJET_API_MESSAGE_PRE_BUILD = 'mailjet_api.message_pre_build';

  const MAILJET_API_MESSAGE_POST_BUILD = 'mailjet_api.message_post_build';

}
